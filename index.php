<?php

// Datenbankverbindung herstellen
  include( "files/mysql.php" );
  $db = new dbc( "files/config.php", $prefix );
  define( "THIS_SELF", "index.php?" );
  
// Sitzung starten
  session_name( "IVSESSID" );
  session_start();
  
  if( isset( $_GET['logout'] )) {
    setcookie( $prefix."id", 0, time() - 1 ); 
    setcookie( $prefix."key", 0, time() - 1 );  
    $_COOKIE = array();
    session_unset();
  }

// Konfiguration laden, alte optionsvariable verf�gbar machen
  foreach( $db->get( $prefix."base_conf" ) as $row )
    $conf[$row['category']][$row['name']] = $row['value'];
  $options = $conf['page'];
  $exchange = array();
  
// Typen auslesen
   foreach( $db->get( $prefix."base_type" ) as $row )
     $types[$row['category']][$row['name']] = $row['id']; 
  
// Einbinden wichtiger Dateien
  $filetypes = array( "class", "function", "include", "library" );
  foreach( $filetypes as $ft )
    foreach( $db->get( $prefix."base_file", "`type` = '".$types['filetype'][$ft]."' AND `inc_front` = 1", -1, "`type`, `id`" ) as $file )
      eval( $file['source'] );

// Funktionsdeklarationen       
  // Fehlerfunktion
    function page_error( $msg ) {
      global $db, $prefix, $types, $conf;
      $error_template  = $db->get( $prefix."content_template", "`type` = '{$types['template']['error']}'", 1 );
      $template_source = $db->get( $prefix."content_template_source", "`style` = '{$conf['system']['style']}' AND `template` = '{$error_template['id']}'", 1 );
       
      if( $template_source ) return str_replace( "{REASON}", $msg, $template_source['source'] );
      else return "Error: ".$msg;
    }
      
  // Sicherheitsfunktion zu Panel includen
    function parse_panel( $panel, $options, $db, $prefix, $layer, $types, $conf, $userdata  ) {    
      global $exchange;
    
      if( !empty( $panel['template'] )) {
        $panel_template  = $db->get( $prefix."content_template", "`id` = '{$panel['template']}' AND `type` = '{$types['template']['panel']}'", 1 );
        $template_source = $db->get( $prefix."content_template_source", "`style` = '{$conf['system']['style']}' AND `template` = '{$panel_template['id']}'", 1 );
        $template = str_replace( "{TITLE}", $panel['name'], $template_source['source'] );
        $template = explode( "{BODY}", $template );
      }
           
      if( $script = $db->id_get( $prefix."base_script", $panel['script'] )) {
        if( $script_source = $db->get( $prefix."base_file", "`id` = '{$script['script']}' AND `type` = '{$types['filetype']['script']}'", 1 )) {
          echo $template[0];
          $panelvars = $db->get_assoc( $prefix."content_variable", "panel = {$panel['id']}", "value", "name" );          
	      eval( $script_source['source'] );
	      echo $template[1];
        } else echo "<b>Fatal Error:</b> Scriptfile {$script['script']} not found.";
      } else echo "<b>Fatal Error:</b> Script {$panel['script']} not found.";
    }
    
  // Sortierfunktion
	function order_elements($a, $b) {
      if ( $a['prio'] == $b['prio'] ) return 0;
      return ( $a['prio'] > $b['prio'] ) ? 1 : -1;
    }
// Ende Funktionsdeklarationen
      
// Sitzung pr�fen
  if( !empty( $_SESSION['user_id'] )) {	
    if( $userdata = $db->id_get( $prefix."user_data", $_SESSION['user_id'] )) {    
      if( $_SESSION['user_ip'] != $_SERVER['REMOTE_ADDR'] ) {
        $userdata = false;
        session_unset();
      } else {
        foreach( $db->fetch_query( "SELECT prf.name, dtl.value FROM ".$prefix."user_details dtl JOIN ".$prefix."user_profil prf ON (dtl.field = prf.id) WHERE dtl.user = '$userdata[id]'" ) as $detail )  
          $userdata['details'][$detail['name']] = $detail['value'];
      }
    } else session_unset();
  }  

// Wartungsmodus
  if( $conf['system']['wartung'] && !( $userdata['type'] & 2 )) {
    echo page_error( $conf['system']['wreason'] );
  } else {
  
    if( isset( $_POST['login_name'] ) && isset( $_POST['login_pass'] )) 
      if( $logindata = $db->get( $prefix."user_data", "name = '$_POST[login_name]'", 1 ))
        if( $logindata['pass'] == crypt( $_POST['login_pass'], $logindata['pass'] ))
          if( $logindata['type'] & 1 ) {	
            $query = array( "last_login" => time(), "last_ip" => $_SERVER['REMOTE_ADDR'] );
            $db->id_update( $prefix."user_data", $query, $logindata['id'] );
            
            $_SESSION['user_id'] = $logindata['id'];
            $_SESSION['user_ip'] = $_SERVER['REMOTE_ADDR'];
            
            if( $_POST['relogin'] ) {
              setcookie( $prefix."id", $logindata['id'], time() + 3600 * 24 * 7 );
              setcookie( $prefix."key", md5( $logindata['pass'] ), time() + 3600 * 24 * 7 ); 
            }
            
            header( "LOCATION: ".THIS_SELF );
            $db->close();
            exit();
          } else $exchange['loginerror'] = "Der User ist nicht f&uuml;r das Backend freigegeben!";
        else $exchange['loginerror']  = "Userdaten ung&uuml;ltig!";
      else $exchange['loginerror']  = "Userdaten ung&uuml;ltig!";

    // Userdaten leeren wenn es kein Frontenduser ist
      if( !( $userdata['type'] & 1 )) unset( $userdata );      
  
	// Aktuelle Seite Laden
	  $page  = (int) $_GET['page'];
	  $layer = $db->get( $prefix."content_layer", "id = '{$page}' AND status != 1", 1 );
      if( $layer['status'] == 2 && $userdata ) unset( $layer );
      if( $layer['status'] == 3 && !$userdata ) unset( $layer );
	  if( !$layer ) $layer = $db->id_get( $prefix."content_layer", $conf['system']['frontpage'] );
  
	// Template ermitteln
	  if( $layer ) {
        $template = $layer['template']; 
		$parent = $layer;
		
		while( empty( $template )) {
		  if( !$parent['parent'] ) {
		    $template = $conf['system']['pagetemplate']; break;  
		  } elseif( $parent = $db->id_get( $prefix."content_layer", $parent['parent'] )) {
		    $template = $parent['template'];
		  } else {
		    $db->close(); die( page_error( "Es gibt Unstimmigkeiten in der Datenbank:<br><i>Inkorrekte Referenz im Layersystem!</i>" ));
	      }   
	    }
	  
		define( "LAYER_SELF", THIS_SELF."page=".$layer['id'] );
		define( "PAGE_SELF", THIS_SELF."page=".$layer['id'] );
		
		if( $source = $db->get( $prefix."content_template_source", "`template` = '{$template}' AND `style` = '{$conf['system']['style']}'", 1 )) {

		  $panelgroups = $db->get( $prefix."content_group" ); 
		  $parent = $layer; 
		  $panels = array();
		  $source = $source['source'];
		  
		// Optionsn ins Template einbinden
		  foreach( $options as $key => $opt )
		    $source = str_replace( "{".strtoupper($key)."}", $opt, $source );
		  
		// Panelgroups Sortieren
		  foreach( $panelgroups as $i => $pg ) { 
		    $panels[$pg['id']] = array();
			$panelgroups[$i]['prio'] = strpos( $source, "{".strtoupper( $pg['token'] )."}" );
		  } usort( $panelgroups, "order_elements" );
		  
		// Panels auslesen
		  while( $parent ) {
		    foreach( $db->get( $prefix."content_panel", "layer = '{$parent['id']}' AND status != 1" ) as $pan )
              if( $pan['status'] == 0 || ( $pan['status'] == 2  && !$userdata ) || ( $pan['status'] == 3  &&  $userdata ))
			    $panels[$pan['group']][] = $pan;
			$parent = $db->id_get( $prefix."content_layer", $parent['parent'] );
		  }
		  
		// Mainlayer Panels auslesen
		  foreach( $db->get( $prefix."content_panel", "layer IS NULL" ) as $pan )
            if( $pan['status'] == 0 || ( $pan['status'] == 2  && !$userdata ) || ( $pan['status'] == 3  &&  $userdata ))
			  $panels[$pan['group']][] = $pan;
	
		// Ausgabe
		  foreach( $panelgroups as $pg )
		    if( $pg['prio'] !== false ) {
			  $ausgabe = explode( "{".strtoupper( $pg['token'] )."}", $source, 2 );
			  echo $ausgabe[0]; $source = $ausgabe[1];
			  usort( $panels[$pg['id']], "order_elements" );
			  
			  foreach( $panels[$pg['id']] as $panel )
				parse_panel( $panel, $options, $db, $prefix, $layer, $types, $conf, $userdata  );
			}
		
		  echo $source;
	
        } else echo page_error( "Es liegt ein interner Fehler vor:<br><i>".htmlspecialchars( "Das Template '{$template}' in Style '{$conf['system']['style']}' kann nicht geladen werden!" ) ."</i>" );
      } else echo page_error( "Es liegt ein interner Fehler vor:<br><i>Die Startseite kann nicht geladen werden!</i>" );
  
  }

// Datenbankverbindung schlie�en
  $db->close();

?>