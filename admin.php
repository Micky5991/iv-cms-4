<?php

// Datenbankverbindung herstellen
  // error_reporting( E_ALL );
  // $startime = microtime();
  include( "files/mysql.php" );
  $db = new dbc( "files/config.php", $prefix );
  define( "IV_SELF", "admin.php?" );
  
// Sitzung starten
  session_name( "IVSESSID" );
  session_start();
  
  if( isset( $_GET['logout'] )) {
    setcookie( $prefix."id", 0, time() - 1 ); 
    setcookie( $prefix."key", 0, time() - 1 ); 
    $_COOKIE = array();
    session_unset();
  }
  
// Konfiguration laden, alte optionsvariable verf�gbar machen
  foreach( $db->get( $prefix."base_conf" ) as $row )
    $conf[$row['category']][$row['name']] = $row['value'];
  $options = $conf['page'];
  
// Typen auslesen
   foreach( $db->get( $prefix."base_type" ) as $row )
     $types[$row['category']][$row['name']] = $row['id']; 
  
// Einbinden wichtiger Dateien
  $filetypes = array( "class", "function", "include", "library" );
  foreach( $filetypes as $ft )
    foreach( $db->get( $prefix."base_file", "`type` = '".$types['filetype'][$ft]."' AND inc_back = 1", -1, "`type`, `id`" ) as $file )
      eval( $file['source'] );
  if( $file = $db->get( $prefix."base_file", "`type` = '".$types['filetype']['adminstyle']."' AND inc_back = 1", 1 ))
  eval( $file['source'] ); else die( "Kein Admin Style gefunden!!!" );  

// Funktion zum beenden der Sitzung
  function terminate_session() {
    global $db;
    session_unset();
    $db->close();
    header( "LOCATION: admin.php" );
    exit();
  }
   
// Abfrage des Logins, initialisierung des Admin Styles   
  $adminstyle = new adminstyle();
  if( !empty( $_SESSION['user_id'] ) && $_SESSION['backend'] && $userdata = $db->id_get( $prefix."user_data", $_SESSION['user_id'] ) ) {

    // Sitzung pr�fen
    if( $userdata['type'] & 2 != 2 ) 
      terminate_session();
    elseif( $userdata['last_ip'] != $_SERVER['REMOTE_ADDR'] || $_SESSION['user_ip'] != $_SERVER['REMOTE_ADDR'] ) 
      terminate_session(); 

    // Userrechte auslesen
    foreach( $db->fetch_query( "SELECT `file`, `flags` FROM ".$prefix."user_role_owner curo JOIN ".$prefix."user_role_flags curf USING ( `role` ) WHERE `user` = '$userdata[id]' AND start_date < UNIX_TIMESTAMP() AND ( end_date > UNIX_TIMESTAMP() OR end_date = 0 )" ) as $recht )
      $userdata['perms'][$recht['file']] = (int) $userdata['perms'][$recht['file']] | $recht['flags']; 
    
    // Usereinstellungen auslesen
    foreach( $db->fetch_query( "SELECT prf.name, dtl.value FROM ".$prefix."user_details dtl JOIN ".$prefix."user_profil prf ON (dtl.field = prf.id) WHERE dtl.user = '$userdata[id]'" ) as $detail )  
      $userdata['details'][$detail['name']] = $detail['value'];
    
    // Funktion zum Men� auslesen  
    function read_menu( $cond = "parent IS NULL" ) {  
      global $db, $prefix, $userdata;
      
      $points = array();
      foreach( $db->get( $prefix."base_menu", $cond ) as $point )
        if( empty($point['file']) )  {
          if( $point['subs'] = read_menu( "parent = $point[id]" ))
            if( !empty( $point['subs']  ))
              $points[] = $point;  
        } elseif( $userdata['perms'][$point['file']] & 1 || $userdata['type'] & 4 ) 
          $points[] = $point;  
        
      return $points;      
    }    
    
    // Men� auslesen
    $adminstyle->menu = read_menu();
    // print_r( $adminstyle->menu );
      
    // Layout ausgeben
    $adminstyle->page_header( "IV Entertainment Adminpanel" );    
      
    // Modul Bestimmen
      $modul = (int) $_GET['modul'];
      define( "MODUL_SELF", IV_SELF."modul=".$modul );	
      
      if( empty( $modul )) {
        foreach( $db->get( $prefix."base_file", "`type` = '".$types['filetype']['frontpage']."' AND inc_back = 1" ) as $file )
		  if( !$file['flags'] || $userdata['perms'][$file['id']] & 1 || $userdata['type'] & 4 )
            eval( $file['source'] );
      } else {
        if( $mod = $db->get( $prefix."base_file", "id = '$modul' AND type = '".$types['filetype']['modul']."'", 1 ))
          if( $userdata['perms'][$mod['id']] & 1 || $userdata['type'] & 4 ) {
            $modulrights = array( "access" => 1 );
	        foreach( $db->get( $prefix."base_file_flag", "`file` = '$mod[id]'", -1, "id" ) as $i => $flag )
              $modulrights[$flag['name']] = $userdata['perms'][$mod['id']] & pow( 2, $i+1 ) || $userdata['type'] & 4;	    

            eval( $mod['source'] );
          } else $adminstyle->msgbox( "Auf das angeforderte Modul kann nicht zugegriffen werden!", "Modulefehler" );
        else $adminstyle->msgbox( "Auf das angeforderte Modul kann nicht zugegriffen werden!", "Modulefehler" );
      }
      
    $adminstyle->page_footer();
      
  } else {
    // Login abfrage
    if( isset( $_POST['admin_name'] ) && isset( $_POST['admin_pass'] ))
      if( $_POST['capture_code'] == $_SESSION['capturecode'] )
        if( $logindata = $db->get( $prefix."user_data", "name = '$_POST[admin_name]'", 1 ))
          if( $logindata['pass'] == crypt( $_POST['admin_pass'], $logindata['pass'] ))
            if( $logindata['type'] & 2  ) {
              $query = array( "last_login" => time(), "last_ip" => $_SERVER['REMOTE_ADDR'] );
              $db->id_update( $prefix."user_data", $query, $logindata['id'] );
              
              $_SESSION['user_id'] = $logindata['id'];
              $_SESSION['user_ip'] = $_SERVER['REMOTE_ADDR'];
              $_SESSION['backend'] = true;
              
              if( $_POST['relogin'] ) {
                setcookie( $prefix."id", $logindata['id'], time() + 3600 * 24 * 7 );
                setcookie( $prefix."key", md5( $logindata['pass'] ), time() + 3600 * 24 * 7 ); 
              }
              
              header( "LOCATION: ".IV_SELF );
              $db->close();
              exit();
            } else $error = "Der User ist nicht f&uuml;r das Backend freigegeben!";
          else $error = "Userdaten ung&uuml;ltig!";
        else $error = "Userdaten ung&uuml;ltig!";
      else $error = "Ung&uuml;ltiger Capture Code!";

    // Ausgabe Login Formular
    $_SESSION['capturecode'] = genpw( 5 );
    $adminstyle->login( "IV Entertainment Adminpanel Login", $error );
  }

// Datenbankverbindung schlie�en */
  $db->close();
  // $ende = microtime();
  // echo "<br><br>". ($ende - $start);

?>