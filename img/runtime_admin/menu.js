openmenus = new Array();

function close_menus( d  ) {
  for( var i = d; i < openmenus.length && openmenus[i] != 0; i++ )
    document.getElementById( 'submenu'+openmenus[i] ).style.display = "none";
}

function show_menu( menuid, anchor ) {
  menu = document.getElementById( 'submenu'+menuid );
  menu.style.left = ( get_x_position( anchor ) - 1  ) + "px";
  menu.style.top  = ( get_y_position( anchor ) + 33 ) + "px";
  menu.style.display = "block";
  openmenus[0] = menuid;
}

function show_submenu( menuid, anchor, d ) {
  close_menus( d );
  menu = document.getElementById( 'submenu'+menuid );
  menu.style.left = ( get_x_position( anchor.offsetParent  ) + 201 ) + "px";
  menu.style.top  = get_y_position( anchor )  + "px";
  menu.style.display = "block";
  openmenus[d] = menuid;
}

function get_x_position( el ) {
  var pos = el.offsetLeft;
  while( ( el = el.offsetParent ) != null ) 
    pos += el.offsetLeft;
  return pos;
}

function get_y_position( el ) {
  var pos = el.offsetTop;
  while( ( el = el.offsetParent ) != null ) 
    pos += el.offsetTop;
  return  pos;
}