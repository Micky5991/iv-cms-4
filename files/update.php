<?php

  class iv_update {
  
    function read_file( $filename ) {
      if( $data = file_get_contents( $filename ))
        return unserialize( urldecode( $data ));
        else return false;
    }
    
    function install_file( $komp, $menu = 0 ) {
      global $db, $prefix;
        
      $query = array( "uid" => $komp['content']['id'], "name" => $komp['content']['name'],
                      "autor" => $komp['content']['autor'], "version" => $komp['content']['version'],
                      "inc_back" => $komp['content']['inc_back'], "inc_front" => $komp['content']['inc_front'],
                      "inc_interface" => $komp['content']['inc_interface'], "flags" => $komp['content']['flags'],
                      "source" => $komp['content']['source'] );
                      
      // Dateityp feststellen / anlegen
      if(( $type = $db->get( $prefix."base_type", "`category` = '{$komp['content']['type']['category']}' AND `name` = '{$komp['content']['type']['name']}'", 1 )) || empty( $komp['content']['type'] ))
        $query['type'] = (int) $type['id'];
      else {
        unset( $komp['content']['type']['id'] );
        $db->insert( $prefix."base_type", $komp['content']['type'] );
        $query['type'] = $db->id();
      }
      
      foreach( $query as $k => $w ) $query[$k] = mysql_real_escape_string( $w );
      
      // Datei anlegen aktuallisieren
      if( $oldfile = $db->get( $prefix."base_file", "uid = '{$komp['content']['id']}'", 1 )) {
        $db->id_update( $prefix."base_file", $query, $oldfile['id'] );
        $fileid = $oldfile['id'];
      } else {
        $db->insert( $prefix."base_file", $query );
        $fileid = $db->id();
      }
        
      // Flags pr�fen / anlegen
      foreach( $komp['content']['flaglist'] as $flag ) {
        unset( $flag['id'] ); $flag['file'] = $fileid;
        foreach( $flag as $k => $w ) $flag[$k] = mysql_real_escape_string( $w );
        
        if( $oldflag = $db->get( $prefix."base_file_flag", "name = '{$flag['name']}' AND file = '$fileid'", 1 ))
          $db->id_update( $prefix."base_file_flag", $flag, $oldflag['id'] );
          else $db->insert( $prefix."base_file_flag", $flag );
      }
        
      // Men�eintrag anlegen
      if( !empty( $menu ) && count( $komp['content']['menuinfo'] ) > 0 )
        if( !$db->get( $prefix."base_menu", "file = '$fileid'", 1 ))  {
          $query = array( "name" => $komp['content']['menuinfo'][0]['name'], "icon" => $komp['content']['menuinfo'][0]['icon'], "parent" => $menu, "file" => $fileid );
          foreach( $query as $k => $w ) $query[$k] = mysql_real_escape_string( $w );
          $db->insert( $prefix."base_menu", $query );
        }
    }
    
    function install_table( $komp, $packageversion = 0 ) {
      global $db, $prefix;
      
      if( $komp['version'] > $packageversion )
        $db->query( str_replace( "cms_", $prefix, $komp['content'] ));
    }
    
    function install_data( $komp, $packageversion = 0 ) {
      global $db, $prefix;
      
      if( $komp['version'] > $packageversion ) {
        foreach( $komp['content'] as $i => $row )
          foreach( $row as $k => $w ) $komp['content'][$i][$k] = mysql_real_escape_string( $w );
        $db->multi_insert( $prefix.$komp['name'], $komp['content'], "REPLACE" );
      }
    }
    
    function install_script( $komp ) {
      global $db, $prefix;
      
      $sfile = $db->get( $prefix."base_file", "`uid` = '{$komp['content']['script']}'", 1 );
      $efile = $db->get( $prefix."base_file", "`uid` = '{$komp['content']['editor']}'", 1 );
      
      if( $sfile && ( $efile || empty( $komp['content']['editor'] ))) {
        $cond = $efile ? "`editor` IS NULL" : "`editor` = '{$efile['id']}'";
        $query = array( "name" => addslashes( $komp['content']['name'] ), "script" => $sfile['id'] );
        if( $efile ) $query["editor"] = $efile['id'] ;
        foreach( $query as $k => $w ) $query[$k] = mysql_real_escape_string( $w );
        
        if( !$db->get( $prefix."base_script", "`script` = '{$sfile['id']}' AND $cond", 1 ))
          $db->insert( $prefix."base_script", $query );
      } else return false;
    }
    
    function install_sql( $komp, $packageversion = 0 ) {
      global $db, $prefix;
      
      if( $komp['version'] > $packageversion )
        $db->query( str_replace( "cms_", $prefix, $komp['content'] ));      
    }
    
    
  }


?>