<?php

class dbc {

  var $link;
  var $mode = false; //"MYSQL40";
  var $commit = false;
  
  function dbc( $config, &$pre ) {
    if( @include( $config ) ) {
      $this->link = @mysql_connect( $sql_host, $sql_user, $sql_pass );
      if (!$this->link) trigger_error( "Could not connect Database!", E_USER_ERROR );
      $db = @mysql_select_db( $sql_db, $this->link );
      if (!$db) trigger_error( "Could not select Database!", E_USER_ERROR );
      
      if( $this->mode ) $this->query( "SET SESSIONSQL_MODE=".$this->mode );      
      $pre = $prefix;
    } else trigger_error( "Config File missing!", E_USER_ERROR );
  }
  
  function query( $sql ) {
    if( $res = mysql_query( $sql, $this->link ))
      return $res;
      else trigger_error( "Bei dem Query: \"".htmlspecialchars( $sql )."\" ist folgender Fehler aufgeterten:<br>".mysql_error(), E_USER_ERROR );
  }

  function get( $table, $cond = 1, $limit = -1, $order = "" ) {
    $query = "SELECT * FROM ".$table." WHERE $cond";   
    if( !empty( $order )) $query .= " ORDER BY $order";
    if( $limit > 0 ) $query .= " LIMIT $limit";
    
    $res = $this->query( $query );
    $result = array();
    while( $row = mysql_fetch_assoc( $res )) $result[] = $row;
    
    mysql_free_result( $res );
    if( $limit == 1 ) return $result[0];
    else return $result;
  }

  function fetch_query( $query ) {
    $res = $this->query( $query );
    $result = array();
    while( $row = mysql_fetch_assoc( $res )) $result[] = $row;
    
    mysql_free_result( $res );
    return $result;
  }

  function get_assoc( $table, $cond = 1, $value, $key = "id" )  {
    $query = "SELECT * FROM ".$table." WHERE $cond ORDER BY $value";
    
    $res = $this->query( $query );
    $result = array();
    while( $row = mysql_fetch_assoc( $res )) $result[$row[$key]] = $row[$value];
    
    mysql_free_result( $res );
    return $result;
  }
  
  function del( $table, $cond ) {
    return $this->query( "DELETE FROM ".$table." WHERE $cond" );
  }
  
  function id_get( $table, $id ) {
    $query = "SELECT * FROM ".$table." WHERE id = '$id' LIMIT 1";
    $res = $this->query( $query );
    $result = mysql_fetch_assoc( $res );
    mysql_free_result( $res );
    return $result;
  }
  
  function id_del( $table, $id ) {
    return $this->query( "DELETE FROM ".$table." WHERE id = '$id' LIMIT 1" );
  }
  
  function id() {
    return @mysql_insert_id( $this->link );
  }

  function close() {
    if( $this->commit ) $db->query( "COMMIT;" );
    mysql_close( $this->link );
  }

  function flist( $table ) {
    $res = $this->query( "SHOW COLUMNS FROM ".$table );
    $fields = array();

    while( $f = mysql_fetch_row( $res )) $fields[] = $f; 
    mysql_free_result( $res );
    return $fields;
  }

  function insert( $table, $values, $type = "INSERT" ) {
    global $userdata;
    if( !empty( $userdata['id'] )) $values['create_by'] = $userdata['id'];
    $values['create_date'] = time();
    
    $fields = array(); $vals = array();
    foreach( $this->flist( $table ) as $f )
      if( isset( $values[$f[0]] )) {
        $fields[] = "`$f[0]`";
        $vals[] = (empty( $values[$f[0]] ) && $f[2] == 'YES') ? "NULL" : "'".$values[$f[0]]."'";
      }

    $this->query( "$type INTO $table ( ".implode( ", ", $fields ).")
                   VALUES ( ".implode( ", ", $vals ).");" );
  }

  function update( $table, $values, $cond = 1 ) {
    global $userdata;
    if( !empty( $userdata['id'] )) $values['update_by'] = $userdata['id'];
    $values['update_date'] = time();
    
    $sql = "UPDATE $table SET "; $ups = array();
    foreach( $this->flist( $table ) as $f )
      if( isset( $values[$f[0]] )) 
        if( empty( $values[$f[0]] ) && $f[2] == 'YES' ) $ups[] = "`$f[0]` = NULL"; 
        else $ups[] = "`$f[0]` = '".$values[$f[0]]."'";      
    $this->query( $sql.implode( ", ", $ups )." WHERE $cond" );
  }  

  function id_update( $table, $values, $id ) {
    $this->update( $table, $values, "id = '".(int)$id."'" );
  }
  
  function multi_insert( $table, $rows, $type = "INSERT" ) {
    global $userdata;
    $flist = $this->flist( $table );
    
    foreach( $rows as $row ) {
      if( !empty( $userdata['id'] )) $row['create_by'] = $userdata['id'];
      $row['create_date'] = time();
    
      $fields = array(); $vals = array();
      foreach( $flist as $f )
      if( isset( $row[$f[0]] )) {
        $fields[] = "`$f[0]`";
        $vals[] = (empty( $row[$f[0]] ) && $f[2] == 'YES') ? "NULL" : "'".$row[$f[0]]."'";
      }

      $this->query( "$type INTO $table ( ".implode( ", ", $fields ).")
                     VALUES ( ".implode( ", ", $vals ).");" );
    }
  }
  
  function multi_update( $table, $rows ) {
    trigger_error( "Function is not implemented!", E_USER_ERROR );  
  }    
}

?>