<?php

  session_name( "IVSESSID" );
  session_start();

  $font = 4;
  $width = imagefontwidth( $font );
  $height = imagefontheight( $font );

  $img = imagecreate( $width * strlen( $_SESSION['capturecode'] ) + 10, $height + 10 );
  $white = imagecolorallocate( $img, 255, 255, 255 );
  $black = imagecolorallocate( $img,   0,   0,   0 );
  imagecolortransparent( $img, $white );
  
  imagestring( $img, $font, 5, 5, $_SESSION['capturecode'], $black );
  header( 'Content-type: image/gif' ); 
  
  imagegif( $img );
  imagedestroy( $img );

?>