<?php

  function formline(  $name, $caption, $value, $type = "text" ) {
    echo '<tr><td width="50%">'.$caption.':</td><td width="50%"><input name="'.$name.'" size="30" value="'.htmlspecialchars( $value ).'" type="'.$type.'"></td></tr>';
  }

  function write_form( $msg = "" ) {
    global $_POST;
    $temp = explode( "{FORM}", file_get_contents( "template.html" ));
    echo $temp[0].'<h3>Installation</h3><div style="font-family:Verdana;">';
	
	if( !empty( $msg )) echo '<p align="center" style="color:red"><b>'.$msg.'</b></p>';
	echo '<form action="index.php" method="post"><table align="center">';
	
	formline( "sql_host", "SQL Host", "localhost" );
	formline( "sql_db", "SQL Datenbank", $_POST["sql_db"] );
	formline( "sql_user", "SQL User", $_POST["sql_user"] );
	formline( "sql_pass", "SQL Passwort", "", "password" );
	formline( "sql_pre", "SQL Prefix", $_POST["sql_pre"] );
	
	echo '<tr><td colspan="2"><hr></td></tr>';
	
	formline( "admin_name", "Adminuser", $_POST["admin_name"] );
	formline( "admin_pass", "Adminpasswort", "", "password" );
	formline( "admin_pass_wh", "Adminpasswort Wiederholung", "", "password" );
	formline( "admin_email", "Admin E-Mail", $_POST["admin_email"] );

	echo '<tr><td colspan="2" align="center"><input type="Submit" value="Installieren"></td></tr></table></div>'.$temp[1];
  }
  
if(!empty($_POST)) {

  // Formularpr�fung
    if(empty($_POST['sql_host']) || empty($_POST['admin_name']) || empty($_POST['admin_pass']) || empty($_POST['admin_pass_wh']) || empty($_POST['sql_user']) || empty($_POST['sql_db']))
      die( write_form( "Bitte f�llen sie alle Felder aus!" ));
    if($_POST['admin_pass']!=$_POST['admin_pass_wh'])
      die(  write_form("Passwort und Passwort Wiederholung stimmen nicht �berein!"));

// Configurationsdatei erzeugen
$text = "<?php\n\n  \$sql_host = '$_POST[sql_host]';\n  \$sql_user = '$_POST[sql_user]';\n
\$sql_pass = '$_POST[sql_pass]';\n  \$sql_db   = '$_POST[sql_db]';\n  \$prefix   = '$_POST[sql_pre]';\n\n?>";

  // Mysql Verbindung herstellen
    $link = mysql_connect($_POST['sql_host'],$_POST['sql_user'],$_POST['sql_pass'])
      or die(write_form("Die angegeben Zugangsdaten sind nicht korrekt!"));
  // Datenbank w�hlen
    if(!mysql_select_db($_POST['sql_db'], $link)) 
      die(write_form("Datenbank nicht gefunden!"));
  
  // Configurationsdatei Schreiben 
	if( $fp = @fopen("../files/config.php","w")) {
	  flock($fp,2);
	  fputs($fp,$text);
	  flock($fp,3);
	  fclose($fp);
	} else die(write_form("Config datei kann nicht angelegt werden: Ordnerrechte fehlerhaft!"));

  // Dateien einbinden
    include( "../files/mysql.php" );
    include( "../files/update.php" );
    $db = new dbc( "../files/config.php", $prefix );
    $update = new iv_update();
    
  // BasisTabellen erstellen  
    $db->query( "CREATE TABLE `$_POST[sql_pre]update_package` (\n  `id` int(10) unsigned NOT NULL auto_increment,\n  `name` varchar(32) NOT NULL,\n  `description` text,\n  `content` text,\n  `source` varchar(255) default NULL,\n  `version` int(10) unsigned NOT NULL,\n  PRIMARY KEY  (`id`)\n) ENGINE=InnoDB DEFAULT CHARSET=latin1" );    
    $db->query( "CREATE TABLE IF NOT EXISTS `rubish_base_file` (\n  `id` int(10) unsigned NOT NULL auto_increment,\n  `uid` int(10) unsigned default NULL,\n  `name` varchar(32) NOT NULL,\n  `autor` varchar(32) default NULL,\n  `type` tinyint(3) unsigned NOT NULL,\n  `source` text,\n  `inc_back` tinyint(1) NOT NULL default '0',\n  `inc_front` tinyint(1) NOT NULL default '0',\n  `inc_interface` tinyint(1) NOT NULL,\n  `flags` tinyint(1) NOT NULL,\n  `create_date` int(10) unsigned default NULL,\n  `create_by` int(10) unsigned default NULL,\n  `update_date` int(10) unsigned default NULL,\n  `update_by` int(10) unsigned default NULL,\n  PRIMARY KEY  (`id`)\n) ENGINE=InnoDB DEFAULT CHARSET=latin1" );
  
  // Installationspakete ausf�hren
  for( $i = 1; $i < 7; $i++ )
    if( !$data = $update->read_file( "package_".$i.".iv4" )) 
      $log .= "Die Datei package_".$i.".iv4 wurde nicht gefunden.";
    elseif( !( is_array( $data ) && is_array( $data['komponents'] ) && !empty( $data['id'] )))
      $log .= "Die Datei package_".$i.".iv4 enth�lt Fehler";
    else {
      
        foreach( $data['komponents'] as $komp )
          switch( $komp['type'] ) {
            case 1: $update->install_file( $komp, $komp['content']['menuinfo'][0]['parent'] ); break;
            case 2: $update->install_table( $komp, 0 ); break;
            case 3: $update->install_data( $komp, 0 ); break;
            case 4: $update->install_script( $komp ); break;
            case 5: $update->install_sql( $komp, 0 ); break;
            default: $log .= "Bei der Installation trat ein Problem auf, eine Komponente konnte nicht erkannt werden!";
         }
          
         $db->insert( $prefix."update_package", $data, "REPLACE" );
         
    } 

  // Admin Account erstellen
    mysql_query( "INSERT INTO `$_POST[sql_pre]user_data` ( `name` , `pass` , `email` , `type` ) VALUES ( '$_POST[admin_name]', '".crypt( $_POST[admin_pass] )."', '$_POST[admin_email]', '7' );", $link );

  // Datenbankverbindung schlie�en erfolgsmeldung ausgeben
    mysql_close( $link );
    $temp = explode( "{FORM}", file_get_contents( "template.html" ));
	echo $temp[0].'<h3>Installation Erfolgreich</h3><div style="font-family:Verdana;">';
	echo 'Ihr IV CMS Fourth Generation wurde erfolgreich installiert.<br>
	      Bitte l�schen sie nun den Ordner "install" von ihrem Webspace!<br>
		</div><div align="right"><input type="button" onclick="window.location.href'.
	     ' = \'../admin.php\'" value="Weiter &raquo;&raquo;"></div>'.$temp[1];

} else write_form();

?>